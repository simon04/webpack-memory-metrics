# Webpack Memory Metrics

The purpose of this project is to track the memory usage of GitLab's Webpack process. Reducing the memory usage of webpack is one goal of GitLab's Webpack working group.

Issue: https://gitlab.com/gitlab-org/gitlab/issues/34093.

Learn more about the Webpack working group at https://about.gitlab.com/company/team/structure/working-groups/webpack/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Running locally](#running-locally)
- [How it works](#how-it-works)
- [CI/CD](#cicd)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Running locally

Run the following command to run locally:

```bash
./script.sh
```

View the chart locally by serving the `/public` folder with a webserver.

For example:

```
cd public
ruby -run -e httpd . -p 9090
```

## How it works

Data is fetched from the `webpack-dev-server` job on the master branch's GitLab pipeline.

This job produces artifacts for a number of data points, such as memory usage and webpack version.

Charts are created with [Chart.js](https://www.chartjs.org/).

## CI/CD

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/), following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).
