const {
  renderIfNotEmptyFactory,
  renderIntroFactory,
  renderCommitStatusFactory,
} = require("./common");

const renderHTMLCommit = ({ web_url, short_id, committed_date } = {}) => {
  if (!web_url) {
    return "";
  }
  return `
<a href="${web_url}">
${short_id} (${committed_date.substr(0, 16)})
</a>`;
};

const renderName = (diff) => {
  const key = `<code>${diff.key}</code>`;
  if (!diff.assets) {
    return key;
  }
  return `
<details>
  <summary>${key}</summary>
  <p>Chunks loaded on the entry point:</p>
  <ul>
    ${diff.assets.map((asset) => `<li>${asset}</li>`).join("\n")}
  </ul>
</details>`;
};

const renderDiffRow = (diff, index) => `
<tr ${index < 10 ? "" : "class='hidden'"}>
  <td>${renderName(diff)}</td>
  <td>${diff.human.before}</td>
  <td>${diff.human.after}</td>
  <td>${diff.human.diff}</td>
  <td>${diff.human.diffPercentage}</td>
</tr>`;

const renderToggler = (
  length,
  id = "checkbox-" + Math.random().toString(36).substring(7)
) =>
  `
  <label for="${id}">Show all ${length} rows</label>
  <input class="toggler" id="${id}" type="checkbox"/>
  `;

const renderHTMLDiffTable = (tableContents) => `
${tableContents.length >= 10 ? renderToggler(tableContents.length) : ""}
<table class="pure-table">
  <thead>
    <tr>
      <th>Entrypoint / Name</th>
      <th>Size before</th>
      <th>Size after</th>
      <th>Diff</th>
      <th>Diff in percent</th>
    </tr>
  </thead>
  <tbody>${tableContents.map(renderDiffRow).join("\n")}</tbody>
</table>
`;

const renderSectionContent = renderIfNotEmptyFactory(
  ({ headline, description, tableContents }) => `
  <h2>${headline}</h2>
  ${description}
  ${renderHTMLDiffTable(tableContents)}
`
);

const renderCommitStatus = renderCommitStatusFactory(renderHTMLCommit);
const renderIntro = renderIntroFactory(renderHTMLCommit);

const renderHTML = ({ before, after, comparison }) => {
  const allEntryPoints = [
    ...comparison.new,
    ...comparison.insignificant,
    ...comparison.growth,
    ...comparison.shrink,
  ].sort((b, a) => a.stats.after - b.stats.after);

  return `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link
    rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pure/2.0.1/pure.min.css"
    integrity="sha256-8k2bI9DxW/KyQRFIULiMek8FDD8wX6wO4T0ejDpPpd4=" crossorigin="anonymous"
  >
  <title>Webpack analysis</title></head>
<body>
<main>
  <h1>Webpack analysis</h1>
  <p>
    ${renderIntro(before, after)}
  </p>
  ${renderCommitStatus(before)}
  ${renderCommitStatus(after)}
  <hr/>
  ${renderSectionContent(comparison, "special")}
  ${renderSectionContent(comparison, "growth")}
  ${renderSectionContent(comparison, "shrink")}
  ${renderSectionContent(comparison, "new")}
  ${renderSectionContent(comparison, "deleted")}
  <h2>Largest entrypoints</h2>
  <p>Below you find all entry points, sorted by size as of</p>
  ${renderHTMLDiffTable(allEntryPoints)}
</main>
</body>
<style>
main {
  margin: 0 auto;
  max-width: 70rem;
  padding: 1rem;
}

table {
  margin: 1rem 0;
}

table td {
  min-width: 6rem;
}

.toggler {
  margin-left: 0.5rem;
}

.toggler:checked + table tr.hidden {
  display: table-row !important;
}
</style>
</html>`;
};

module.exports = renderHTML;
