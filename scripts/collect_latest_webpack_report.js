const fs = require("fs");
const path = require("path");
const moment = require("moment");
const CommitIterator = require("../lib/commit_iterator");
const { retrieveLatestArtifact, STATUS } = require("../lib/common");

async function main() {
  const allCommits = new CommitIterator({
    firstTimeStamp: moment().subtract("36", "h"),
  });

  for await (let commit of allCommits) {
    const { id, created_at } = commit;

    console.log(`Checking commit ${id}`);

    const { status, content } = await retrieveLatestArtifact(
      id,
      ["compile-production-assets", "gitlab:assets:compile pull-cache"],
      "webpack-report/index.html",
      (stream) =>
        new Promise((resolve, reject) => {
          let res = "";
          stream.on("data", (chunk) => {
            res += chunk;
          });
          stream.on("end", () => {
            const html = res
              /**
               * Ensure that the sidebar is opened
               */
              .replace(
                "this.toggleVisibility(!1),3e3)",
                "this.toggleVisibility(!0),3e3)"
              )
              .replace("pinned:!1", "pinned:!0")
              .replace("sidebarPinned:!1", "sidebarPinned:!0")
              .replace(
                "</body>",
                `
                <script
                  id="injected-script"
                  src="./last-report.js"
                  type="application/javascript"
                  data-commit="${id}"
                  data-created="${created_at}"></script>
                </body>`
              );

            resolve(html);
          });
        })
    );

    if (status !== STATUS.SUCCESS) {
      continue;
    }

    fs.writeFileSync(
      path.join(__dirname, "..", "public", "last-report.html"),
      content,
      "utf-8"
    );

    break;
  }
}

main()
  .then((result) => {
    console.warn("Successfully loaded latest webpack bundle report!");
  })
  .catch((e) => {
    console.warn("An error happened!");
    console.warn(e);
    process.exit(1);
  });
