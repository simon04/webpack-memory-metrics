const {
  renderIfNotEmptyFactory,
  renderCommitStatusFactory,
  renderIntroFactory,
  sections,
} = require("./common");

const FoundationTEAM = ["dmishunov", "justin_ho", "mikegreiling", "nmezzopera"];

const renderMarkdownCommit = ({ id } = {}) => id;

const renderMarkdownRow = (diff) =>
  `| ${diff.key} | ${diff.human.before} | ${diff.human.after} | ${diff.human.diff} | ${diff.human.diffPercentage} |`;

const renderFullReport = (fullReport = false) =>
  fullReport
    ? `Please look at the [full report](${fullReport}) for more details`
    : "";

const tableNote = (fullReport, boolean) =>
  fullReport && boolean
    ? `The table above is limited to 10 entries. ${renderFullReport(
        fullReport
      )}`
    : "";

const renderMarkdownTable = (tableContents) => `
| Entrypoint / Name | Size before | Size after | Diff | Diff in percent |
| --- | --- | --- | --- | --- |
${tableContents.slice(0, 10).map(renderMarkdownRow).join("\n")}
`;

const renderCommitStatus = renderCommitStatusFactory(renderMarkdownCommit);

const renderSection = renderIfNotEmptyFactory(
  ({ headline, emoji, tableContents, fullReport }) =>
    `
### ${emoji} ${headline}: ${tableContents.length}

<details>
<summary>Expand</summary>
  
${renderMarkdownTable(tableContents)}

${tableNote(fullReport, tableContents.length > 10)}

</details>`
);

const renderFoundationTeam = () => {
  const [last, ...team] = FoundationTEAM.sort()
    .reverse()
    .map((x) => `\`@${x}\``);
  return team.reverse().join(", ") + " or " + last;
};

function renderPing(comparison, { thresholdSize, thresholdPercent } = {}) {
  if (comparison.growth.length) {
    return [
      `Your MR has at least one entrypoint growing significantly (more > ${thresholdSize} or ${thresholdPercent}).`,
      `If you write new or extend existing features, this is expected and there is nothing to worry about.`,
      `\n**Please consider pinging** someone from the FE Foundations (${renderFoundationTeam()}) for review, if you are unsure about the size increase.`,
    ].join("\n");
  }

  return "";
}

const renderIntro = renderIntroFactory(renderMarkdownCommit);

const renderMarkdown = ({ before, after, comparison, ...args }) =>
  `## Bundle size analysis [beta]

${renderIntro(before, after)}

### ${sections.special.emoji} ${sections.special.headline}
${renderMarkdownTable(comparison.special)}
${renderSection(comparison, "growth", args)}
${renderSection(comparison, "new", args)}
${renderSection(comparison, "shrink", args)}

- - -

${renderPing(comparison, args)}

${renderCommitStatus(before)}
${renderCommitStatus(after)}
${renderFullReport(args.fullReport)}

- - -

[Read more](https://gitlab.com/gitlab-org/frontend/playground/webpack-memory-metrics/-/blob/master/doc/report.md) about how this report works.

  `;

module.exports = renderMarkdown;
