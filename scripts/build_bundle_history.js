const path = require("path");
const fs = require("fs");
const moment = require("moment");
const history = require("../webpack-bundle-sizes/bundle-history.json");

let lastDate;

const mapped = history
  .reverse()
  .flatMap((commit) => {
    if (!commit.hasStats) {
      return [];
    }

    if (lastDate && moment(commit.created_at).isAfter(lastDate)) {
      return [];
    }

    try {
      const stats = require(path.join(
        __dirname,
        "..",
        "webpack-bundle-sizes",
        "bundleStorage",
        commit.id + ".json"
      ));

      const main = stats.statistics.mainChunk.size;
      const average = stats.statistics.average.size;

      const date = moment(commit.created_at);
      lastDate = moment(date).subtract(24, "h");

      return [
        {
          date: date.unix(),
          main,
          average,
          commitSHA: commit.id,
        },
      ];
    } catch (e) {
      return [];
    }
  })
  .sort((b, a) => a.date - b.date);

fs.writeFileSync(
  path.join(__dirname, "..", "public", "history-bundle-size.json"),
  JSON.stringify(mapped, null, 2)
);
