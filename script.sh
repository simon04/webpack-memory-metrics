#!/bin/bash
# Unofficial bash mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

DATA_REPO="gitlab.com/gitlab-org/frontend/playground/webpack-bundle-sizes.git"
HAS_CI=${CI:=false}

if [ "${HAS_CI}" == "true" ]; then
  git clone "https://$DATA_REPO" webpack-bundle-sizes
fi

echo "Trying to download latest build memory results from gitlab-org/gitlab"
node ./scripts/build_memory_history.js

echo "Trying to download latest bundle sizes from gitlab-org/gitlab"
mkdir -p webpack-bundle-sizes/bundleStorage/
node ./scripts/collect_bundle_stats.js --expose-gc

echo "Build bundle history for the webpage"
node ./scripts/build_bundle_history.js

echo "Retrieve latest webpack report"
node ./scripts/collect_latest_webpack_report.js --expose-gc

if [ "${HAS_CI}" == "true" ] && [ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]; then
  echo "Committing changes to the data"
  git config --global user.email "gitlab-bot@gitlab.com"
  git config --global user.name "GitLab Bot"
  cd webpack-bundle-sizes
  if test -n "$(git status --porcelain)"; then
    git add .
    git commit -m "Update data"
    git push -u "https://gitlab-bot:${GITLAB_TOKEN}@${DATA_REPO}"
  fi
fi
