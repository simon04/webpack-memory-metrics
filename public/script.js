window.addEventListener("DOMContentLoaded", async () => {
  const response = await fetch("./history-memory-size.json");
  const history = await response.json();

  const response2 = await fetch("./history-bundle-size.json");
  const bundleSize = await response2.json();

  var sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  function formatSize(input, b = 2) {
    const a = Math.abs(input);
    if (0 === a) {
      return "0 Bytes";
    }
    const c = 0 > b ? 0 : b,
      d = Math.floor(Math.log(a) / Math.log(1024));
    return parseFloat((a / Math.pow(1024, d)).toFixed(c)) + " " + sizes[d];
  }

  const ONE_KIB = 1024;
  const ONE_MIB = 1024 * 1024;

  var ctx = document.getElementById("memoryChart").getContext("2d");
  new Chart(ctx, {
    type: "line",
    data: {
      datasets: [
        {
          label: "Memory Used by Webpack Dev Server",
          data: history.map((entry) => ({
            x: entry.date,
            y: entry.memoryUsage,
          })),
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          borderColor: "rgba(255, 99, 132, 1)",
          borderWidth: 1,
        },
      ],
    },
    options: {
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            let label = data.datasets[tooltipItem.datasetIndex].label || "";

            if (label) {
              label += ": ";
            }
            label += formatSize(tooltipItem.yLabel);
            return label;
          },
        },
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              tooltipFormat: "ll HH:mm",
            },
            scaleLabel: {
              display: true,
              labelString: "Date",
            },
          },
        ],
        yAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: "Memory in MiB",
            },
            ticks: {
              min: 500 * ONE_MIB,
              suggestedMax: 1200 * ONE_MIB,
              stepSize: 100 * ONE_MIB,
              beginAtZero: true,
              callback: formatSize,
            },
          },
        ],
      },
    },
  });

  var ctx2 = document.getElementById("bundleChart").getContext("2d");
  new Chart(ctx2, {
    type: "line",
    data: {
      datasets: [
        {
          label: "Main Chunk size",
          data: bundleSize.map((entry) => ({
            x: entry.date * 1000,
            y: entry.main,
          })),
          backgroundColor: "rgba(255, 99, 132, 0.2)",
          borderColor: "rgba(255, 99, 132, 1)",
          borderWidth: 1,
          fill: false,
        },

        {
          label: "Average entry point size",
          data: bundleSize.map((entry) => ({
            x: entry.date * 1000,
            y: entry.average - entry.main,
          })),
          backgroundColor: "rgba(99,156,255, 0.2)",
          borderColor: "rgb(99,156,255)",
          borderWidth: 1,
          fill: false,
        },
      ],
    },
    options: {
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            let label = data.datasets[tooltipItem.datasetIndex].label || "";

            if (label) {
              label += ": ";
            }
            label += formatSize(tooltipItem.yLabel);
            return label;
          },
        },
      },
      scales: {
        xAxes: [
          {
            type: "time",
            time: {
              tooltipFormat: "ll HH:mm",
            },
            scaleLabel: {
              display: true,
              labelString: "Date",
            },
          },
        ],
        yAxes: [
          {
            type: "logarithmic",
            scaleLabel: {
              display: true,
              labelString: "Memory in MiB",
            },
            ticks: {
              max: 8 * ONE_MIB,
              beginAtZero: true,
              callback: formatSize,
            },
            afterBuildTicks: (chartObj) => {
              const ticks = [
                0,
                128 * ONE_KIB,
                256 * ONE_KIB,
                512 * ONE_KIB,
                1 * ONE_MIB,
                2 * ONE_MIB,
                4 * ONE_MIB,
                8 * ONE_MIB,
              ];
              chartObj.ticks.splice(0, chartObj.ticks.length);
              chartObj.ticks.push(...ticks);
            },
          },
        ],
      },
    },
  });
});
