const style = document.createElement("link");
style.setAttribute("rel", "stylesheet");
style.setAttribute("href", "./last-report.css");

const script = document.createElement("script");
script.setAttribute(
  "src",
  "https://cdnjs.cloudflare.com/ajax/libs/timeago.js/4.0.2/timeago.min.js"
);
script.setAttribute(
  "integrity",
  "sha512-SVDh1zH5N9ChofSlNAK43lcNS7lWze6DTVx1JCXH1Tmno+0/1jMpdbR8YDgDUfcUrPp1xyE53G42GFrcM0CMVg=="
);
script.setAttribute("crossorigin", "anonymous");

script.onload = () => {
  console.log("ONLAOD");
  console.log(window.timeago);
  timeago.render(document.querySelectorAll("footer.sticky-footer abbr"));
};

const footer = document.createElement("footer");
footer.classList.add("sticky-footer");
const { commit, created } = document.querySelector("#injected-script").dataset;

footer.innerHTML = `
  Webpack Bundle Size analysis based on commit <a href="https://gitlab.com/gitlab-org/gitlab/commit/${commit}">${commit}</a>
  (<abbr title="${created}" datetime="${created}">${created}</abbr>)`;

document.body.append(footer);
document.head.append(style, script);
