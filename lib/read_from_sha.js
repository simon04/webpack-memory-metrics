const { gitlabAPI } = require("./common");

let commitMapping;

const PROJECT_ID = 18250019;

const startDate = "2020-03-20T20:06:35.000+00:00";

module.exports = async function readFromSHA(sha) {
  if (!commitMapping) {
    console.warn("Retrieving commit mapping");
    const { data } = await gitlabAPI.get(
      `https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files/bundle-commits%2Etxt/raw?ref=master`
    );
    commitMapping = data;
  }
  const entries = commitMapping
    .split("\n")
    .reverse()
    .map((x) => x.split("\t"));

  const latest = entries[0][1];

  let match = Object.fromEntries(entries)[sha];
  let commitStatus = null;

  if (sha !== match) {
    console.warn(`Let's gather more info around ${sha}`);
    const { data } = await gitlabAPI.get(`/repository/commits/${sha}`);

    commitStatus = data;

    const committedDate = data.committed_date;

    if (committedDate < startDate) {
      throw new Error(
        `Commit ${sha} is too old (${committedDate}). We just have data starting with ${startDate}`
      );
    }

    if (!match) {
      match = latest;
      commitStatus.reason = "TOO_NEW";
    } else {
      commitStatus.reason = "NO_PIPELINE";
    }
  }

  console.warn(`Downloading data for ${match}`);

  const { data } = await gitlabAPI.get(
    `https://gitlab.com/api/v4/projects/${PROJECT_ID}/repository/files/bundleStorage%2F${match}%2Ejson/raw?ref=master`
  );

  return {
    commitStatus,
    ...data,
  };
};
